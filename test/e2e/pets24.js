var config = require('../../nightwatch.conf.js');



module.exports = {

    'Pets24': function(browser) {

        browser

            .url('https://www.pets24.ee/')

            .waitForElementVisible('body')

            .assert.title('Loomapood. Lemmikloomakaubad - koertele, kassidele ja teistele - Pets24.ee')

            .windowSize('current', 1300, 1000)

            .saveScreenshot(config.imgpath(browser) + 'pets24-esileht.png')

            .assert.containsText(".nav1 a", "Küsimused ja Vastused")

            .click('.nav1 a:nth-child(1)')

            .waitForElementVisible('body', 2000)

            .saveScreenshot(config.imgpath(browser) + 'pets24_kusimusedjavastused.png')

            .assert.elementPresent('.main h1')

            .assert.containsText(".main h1", "KKK")

            .assert.title('KKK - Pets24.ee')

            .click('.logo')

            .useXpath()

            .click("/html/body/div[3]/div/div[2]/div[1]/div/div[4]/div[2]/input[1]")

            .useCss()

            .click(".inner-cart a")

            .click(".search-bottom")

            .saveScreenshot(config.imgpath(browser) + 'pets24_ostukorv.png')

            .setValue('input[type=text]', 'farmina')

            .click(".button-big")

            .assert.containsText(".filter_block", "Kuva ka need kaubad, mida tellida ei saa")

            .pause(200)

            .saveScreenshot(config.imgpath(browser) + 'pets24_otsingutulemused.png')

            .end();
    }

};